# Build docker images for proofgeneral/coq-emacs

[![tags](https://img.shields.io/badge/tags%20on-docker%20hub-blue.svg)](https://hub.docker.com/r/proofgeneral/coq-emacs#supported-tags "Supported tags on Docker Hub")
[![pipeline status](https://gitlab.com/hendriktews/coq-emacs-docker/badges/master/pipeline.svg)](https://gitlab.com/hendriktews/coq-emacs-docker/-/pipelines)
[![pulls](https://img.shields.io/docker/pulls/proofgeneral/coq-emacs.svg)](https://hub.docker.com/r/proofgeneral/coq-emacs "Number of pulls from Docker Hub")
[![stars](https://img.shields.io/docker/stars/proofgeneral/coq-emacs.svg)](https://hub.docker.com/r/proofgeneral/coq-emacs "Star the image on Docker Hub")  
[![dockerfile](https://img.shields.io/badge/dockerfile%20on-gitlab-blue.svg)](https://gitlab.com/hendriktews/coq-emacs-docker "Dockerfile source repository")
[![coq](https://img.shields.io/badge/depends%20on-coqorg%2Fcoq-blue.svg)](https://hub.docker.com/r/coqorg/coq "Docker images of Coq")

This repository provides [Docker](https://www.docker.com/) images
containing [Coq](https://coq.inria.fr/) and
[Emacs](https://www.gnu.org/software/emacs/) in various version
combinations. 

## Policy for building Coq/Emacs combinations

Build all Emacs versions Proof General supports with all Coq
version from the last 2 years. Additionally, build the Emacs
versions in Ubuntu LTS versions with standard support and in
live Debian versions with all Coq versions since the release of
these Ubuntu and Debian versions. More precisely

- In 2021/11 Proof General supports Emacs 25.1 or older. The Coq
  versions of the last 2 years go back until 8.10. Therefore
  build the full matrix of all versions of Emacs 25.1-27.2 and
  Coq 8.10-8.14. This gives 40 combinations. If in the future the
  number of combinations becomes larger than 50, then the time
  span of 2 years will be reduced.
  
- In 2021/11 there is only one Ubuntu LTS version before its end
  of standard support: 18.04 Bionic Beaver. Bionic was released
  with Emacs 25.2 and Coq 8.6. For those that use Bionic with a
  more recent Coq version, build Emacs 25.2 with all Coq versions
  since 8.6. Note that Coq versions after 8.10 are covered by the
  first point already.
  
- In 2021/11 the only Debian version before its end of live date
  is Debian 11 Buster. It was release with Emacs 26.2 and Coq
  8.9.0. For those that use Buster with a more recent Coq
  version, build Emacs 26.2 with all Coq versions since 8.9.0.
  Note that Coq versions after 8.10 are covered by the first
  point already.

## Image and Repo basis

These images are based on the
[proofgeneral/coq-nix](https://hub.docker.com/r/proofgeneral/coq-nix/)
images, itself based on [Debian 10
Slim](https://hub.docker.com/_/debian/) and use the [Nix Emacs
versions](https://github.com/purcell/nix-emacs-ci/) build for
continuous integration.

<!--
 !-- See also the [docker-coq wiki](https://github.com/coq-community/docker-coq/wiki) for details about how to use such images locally or in a CI context.
  -->

<!-- tags -->

This repository incorporates the
[docker-keeper](https://gitlab.com/erikmd/docker-keeper) tool as a
[subtree](https://www.atlassian.com/git/tutorials/git-subtree).

For more details, see the documentation available in the
[docker-keeper wiki](https://gitlab.com/erikmd/docker-keeper/-/wikis/home).
